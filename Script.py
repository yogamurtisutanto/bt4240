
# coding: utf-8

# # Project Code

# ## Import Libraries

# In[1]:


import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression


# ## Read Data

# In[2]:


# Train
train_data_labels = pd.read_csv('Dataset/Train/train_labels.csv')
train_data_SBM = pd.read_csv('Dataset/Train/train_SBM.csv')
train_data_FNC = pd.read_csv('Dataset/Train/train_FNC.csv')
train_data_mrg = pd.merge(train_data_labels, train_data_SBM, 'outer', on = 'Id')
train_data = pd.merge(train_data_mrg, train_data_FNC, 'outer', on = 'Id')
## Remove id
train_data = train_data.iloc[:,1:]

# Test
test_data_SBM = pd.read_csv('Dataset/Test/test_SBM.csv')
test_data_FNC = pd.read_csv('Dataset/Test/test_FNC.csv')
test_data = pd.merge(test_data_SBM, test_data_FNC, 'outer', on='Id')

test_data.to_csv('/artifacts/' + 'test' + '.csv', index = False)

x = train_data[['SBM_map67', 'FNC220', 'SBM_map17', 'FNC189', 'FNC279', 'FNC313']]
y = train_data[['Class']]
y_values = y.values.ravel()
x_ans = test_data[['SBM_map67', 'FNC220', 'SBM_map17', 'FNC189', 'FNC279', 'FNC313']]

logistic_regression = LogisticRegression()
logistic_regression = logistic_regression.fit(x, y)
classifier_prediction = logistic_regression.predict_proba(x_ans)[:,1]
print(classifier_prediction)
from sklearn.externals import joblib
joblib.dump(logistic_regression, '/artifacts/logistic_regression.joblib') 